// ******************** //
//   Require packages  //
// ****************** //

const fs = require('fs'),
request = require('request'),
telegramBot = require('node-telegram-bot-api'),
speech = require('@google-cloud/speech'),
moment = require('moment');

// ************************ //
//   Setup Env Variables   //
// ********************** //

require('dotenv').config();
const token = process.env.TEL_TOKEN;


// *********************** //
//   Setup Telegram Bot   //
// ********************* // 
 
const bot = new telegramBot(token, {polling: true});

bot.on('voice', (msg) => {

  console.log("Voice message received");
    
  var file_id = msg.voice.file_id;
  var fileName = file_id;
  var file = bot.getFile(file_id);
  var user_id = msg.chat.id;

  file.then(function (result) {

    console.log("File is ready, let's download it!");
    
    var file_path = result.file_path;
    var voice_url = `https://api.telegram.org/file/bot${token}/${file_path}`
    var downloadFilePath = `${fileName}.oga`;

    download(voice_url, fileName, downloadFilePath, function(err){
      
      var emojiList = ["🙈", "🙉", "🙊", "🐵", "🐶", "🦊", "🐱", "🦁", "🐯", "🐴", "🦄", "🐭", "🐹", "🐰", "🐻", "🐨", "🐼", "🐣", "🐤", "🐦", "🐧", "🐸", "🐲", "🐳", "🐋", "🦋"];
      var emojiLogo = emojiList[Math.floor(Math.random()*emojiList.length)];
                
      createTranscript(downloadFilePath, user_id, emojiLogo);

    });
    
  });

});


// **************************************** //
//   Download function for the audio file  //
// ************************************** //

var download = function(uri, filename, downloadFilePath, callback){

  request.head(uri, function(err, res, body){
    request(uri).pipe(fs.createWriteStream(downloadFilePath)).on('close', callback);
  });

};


// *************************************** //
//   Create transcript from audio file :) //
// ************************************* //

function createTranscript(filePath, user_id, emoji) {
 
  console.log("Transcribing...");

  const client = new speech.SpeechClient({
    keyFilename: 'auth.json'
  });

  // Reads a local audio file and converts it to base64
  const file = fs.readFileSync(filePath);
  const audioBytes = file.toString('base64');

  // The audio file's encoding, sample rate in hertz
  const audio = {
    content: audioBytes,
  };

  // The Google Cloud API supports different language codes.
  // For a complete overview see: https://cloud.google.com/speech-to-text/docs/languages
  const config = {
    encoding: 'OGG_OPUS',
    sampleRateHertz: 16000,
    languageCode: 'en'
  };

  const request = {
    audio: audio,
    config: config,
  };

  // Make the transcript from the audio
  client
  .recognize(request)
  .then(data => {
    const response = data[0];
    var transcription = response.results
     .map(result => result.alternatives[0].transcript)
     .join('\n');
    transcription = transcription.replace(/\bparagraph \b/g, '\n\n');
    const journalDate = moment().format("dddd Do MMMM");
    bot.sendMessage(user_id, `<b>${emoji} ${journalDate}</b>\n\n${transcription} `, { parse_mode: "HTML" });

    // Remove the audio file after making and sending the transcript
    fs.unlink(filePath, (err) => {
      if (err) throw err;
      console.log('The transcript was send and the audio file was deleted!');
    });


  })
  .catch(err => {
   console.error('ERROR:', err);
  });

};