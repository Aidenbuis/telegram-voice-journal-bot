# Speech-To-Text Journal Telegram Bot

Hi There, My name is [Aiden](https://twitter.com/aidenbuis) :)

I am an Indie Maker and Freelance Web Developer. I recently made [Open Habits](https://openhabits.com/) to help you form new habits and [LucidDreamBot](https://luciddreambot.com/), which helps you to achieve lucid dreams. I log every step of my journey as an indie maker on [my Twitter](https://twitter.com/aidenbuis).

After building LucidDreamBot I saw the power of using voice and I decided to make a voice-to-text journal chatbot. I tried to make this readme as clear and concise as possible and hope it inspires people to learn programming.

It is built on Telegram and the Google Cloud Text-to-Speech API. The costs of using the Google transcription API is $0,006 per 15 seconds of audio, but after signing up you receive $300 in [free credits](https://cloud.google.com/free/), which you can use for 12 months. 💸 With that you can save 208 hours of you journaling! ✍️ 

## Getting Started

These instructions will get your personal journaling chatbot up and running. The only thing You will need is a server to host the script and get a Google API and Telegram Bot API key. 

Personally I use a [Digital Ocean](https://digitalocean.com) $5/m VPS, which hosts not only this script, but all my websites. You can also use a PaaS like Heroku, but setting up a VPS yourself isn't that hard as Digital Ocean has great documentation!

Now let's dive in!

## Installing Node.js

To run the Telegram Bot locally you need Node.js which you can install [here]([https://nodejs.org/en/download/](https://nodejs.org/en/download/))

## Pull and install the node dependencies
Pull or download this repository. After you've done that open your terminal, navigate to the folder

    ~ cd folder/folder/bot_folder

And type the following command to install the node dependencies. You need the NPM package manager for this, which you can install [here](https://docs.npmjs.com/downloading-and-installing-node-js-and-npm)

    ~ npm install


## Creating your chatbot

You will need to make a Telegram chatbot. You can do this by opening up [BotFather]([http://t.me/botfather](http://t.me/botfather)) and typing

    /newbot

After giving it a name you will receive a API key. Make a new file in the main directory called '.env'. You can do this in the command line with, where the ~ indicates the command is run in a CLI (Command Line Interface).

    ~ touch .env

In this file you paste your Telegram bot API key with the following format. In the command line you can do this by typing.

    ~ nano .env

    TEL_TOKEN=123456789:ABCDEFGHIJKLMNOPQRSTUVWXYABDCDEFGHI

To exit the nano text editor press Ctrl + X. Then press 'y' to save and exit. Now the script can connect to your Telegram bot.

## Connecting the bot to the Google Cloud API

Sign up for Google Cloud [here]([https://cloud.google.com/free/](https://cloud.google.com/free/)) and receive your free $300 in credits. After that go the the [Google Developers Console]

Then search for Cloud Speech API and enable it.

After that [Follow these steps]([https://cloud.google.com/docs/authentication/getting-started#creating_a_service_account](https://cloud.google.com/docs/authentication/getting-started#creating_a_service_account)) to create an auth.json file. After downloading the file put it in the main directory. The script will use this to connect to the Google Cloud API and transcribe the audio.

## Running the chatbot

After that you are ready to run the bot! You can do this locally by opening up the terminal and go to the directory. To do this you can use

    ~ cd folder/folder/bot_folder

After that you can run the bot with

    ~ node speech.js

Now open up the bot you've created. You can find a link to your bot in the same message that contains your API key that BotFather send you. After opening your bot you are ready to send the voice message!

## Keeping the bot alive

With the 'speech.js' script running your bot is alive! But if you shutdown your computer it will die :'(. To keep it alive you can deploy it to your server. Then you can use a process manager like [PM2]([https://pm2.io](https://pm2.io/)) to keep it alive and running.

After deploying to your server make sure to run 'npm install' again to install the needed dependencies.

## Questions

That is it! If you have any questions feel free to reach out to me on [Twitter](https://twitter.com/aidenbuis)